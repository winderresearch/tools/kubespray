
variable "mlflow_namespace" {
  description = "(Optional) The namespace to install into."
  type        = string
  default     = "mlflow"
}

resource "kubernetes_namespace" "mlflow_namespace" {
  metadata {
    name = var.mlflow_namespace
  }
}

module "mlflow" {
  source    = "combinator-ml/mlflow/k8s"
  version   = "0.0.4"
  namespace = kubernetes_namespace.mlflow_namespace.id
}
